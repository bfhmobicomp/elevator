package app.view.controller;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.chart.LineChart;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Robin on 09.04.15
 */
public class Controller {
    private static final int MAX_DATA_POINTS = 1000;

    private XYChart.Series<Number, Number> barometricAltitudeSeries;
    private int barometricAltitudeXSeriesDataPosition = 0;

    private static ConcurrentLinkedQueue<Number> dataBarometricAltitude = new ConcurrentLinkedQueue<Number>();

    @FXML
    private Button btnStart;

    @FXML
    private AnchorPane paneAltitude;

    @FXML
    private Label lblAirPressure;

    @FXML
    private AnchorPane paneAirPressure;

    @FXML
    private Label lblAltitude;

    @FXML
    private LineChart<Number, Number> chrtAltitude;

    @FXML
    private NumberAxis xAxis;

    @FXML
    private NumberAxis yAxis;

    @FXML
    private void initialize() {
        this.init();
        this.prepareTimeline();

        this.lblAirPressure.setText("0 hPa");
        this.lblAltitude.setText("0 m");
    }

    // Timeline gets called in the JavaFX Main thread
    private void prepareTimeline() {
        // Every frame to take any data from queue and add to chart
        new AnimationTimer() {
            @Override
            public void handle(final long now) {
                Controller.this.addDataToSeries();
            }
        }.start();
    }

    public static void addBarometricAltitudeData(final double data) {
        Controller.dataBarometricAltitude.add(data);
    }

    public void init() {
        this.xAxis = this.initXAxis();
        this.yAxis = this.initYAxis();
        this.chrtAltitude = this.initChart();

        // Chart Series
        this.barometricAltitudeSeries = new LineChart.Series<Number, Number>();
        this.barometricAltitudeSeries.setName("Barometric Altitude");
        this.chrtAltitude.getData().add(this.barometricAltitudeSeries);
    }

    private NumberAxis initXAxis() {
        final NumberAxis xAxis = new NumberAxis(0,
                Controller.MAX_DATA_POINTS,
                Controller.MAX_DATA_POINTS / 10);

        xAxis.setTickLabelFont(Font.font("Arial", FontWeight.MEDIUM, 18));
        xAxis.setForceZeroInRange(false);
        xAxis.setAutoRanging(false);

        return xAxis;
    }

    private NumberAxis initYAxis() {
        final NumberAxis yAxis = new NumberAxis();

        yAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(yAxis) {
            @Override
            public String toString(final Number object) {
                return String.format("%6.2f", object);
            }
        });
        yAxis.setTickLabelFont(Font.font("Arial", FontWeight.MEDIUM, 18));
        yAxis.setPrefWidth(120);
        yAxis.setAutoRanging(true);
        yAxis.setLabel("Meters⁻²");
        yAxis.setForceZeroInRange(false);
        yAxis.setAnimated(true);

        return yAxis;
    }

    private LineChart<Number, Number> initChart() {
        final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(
                this.xAxis, this.yAxis) {
            // Override to remove symbols on each data point
            @Override
            protected void dataItemAdded(final Series<Number, Number> series,
                                         final int itemIndex, final Data<Number, Number> item) {
            }
        };
        lineChart.setAnimated(false);
        lineChart.setTitle("Barometric (Altitude)");

        return lineChart;
    }

    private void addDataToSeries() {
        for (int i = 0; i < 100; i++) { // -- add some new samples to the plot
            if (Controller.dataBarometricAltitude.isEmpty()) {
                break;
            }
            this.barometricAltitudeSeries
                    .getData()
                    .add(new LineChart.Data<Number, Number>(
                            this.barometricAltitudeXSeriesDataPosition++,
                            Controller.dataBarometricAltitude.remove()));
        }

        // remove points to keep us at no more than MAX_DATA_POINTS
        if (this.barometricAltitudeSeries.getData().size() > Controller.MAX_DATA_POINTS) {
            this.barometricAltitudeSeries.getData().remove(
                    0,
                    this.barometricAltitudeSeries.getData().size()
                            - Controller.MAX_DATA_POINTS);
        }
        // update Axis
        this.xAxis.setLowerBound(this.barometricAltitudeXSeriesDataPosition
                - Controller.MAX_DATA_POINTS);
        this.xAxis.setUpperBound(this.barometricAltitudeXSeriesDataPosition - 1);

        if(!Controller.dataBarometricAltitude.isEmpty()) {
            this.lblAltitude.setText(Controller.dataBarometricAltitude.poll().toString());
        }
    }

}

