package app.view;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Robin on 09.04.15
 */
public class GUIApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/elevator.fxml"));
        primaryStage.setTitle("Elevator Application");
        primaryStage.setScene(new Scene(root, 600, 800));
        primaryStage.show();
    }
}
