package app;

import app.view.controller.Controller;
import com.tinkerforge.*;
import com.uahnn.tinkerforge.tinker.agent.implementation.TinkerforgeStackAgent;
import com.uahnn.tinkerforge.tinker.application.implementation.AbstractTinkerforgeApplication;
import com.tinkerforge.BrickletBarometer.*;
import com.uahnn.tinkerforge.tinker.core.implementation.TinkerforgeDevice;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Robin on 03.04.15
 */
public class AltitudeApplication extends AbstractTinkerforgeApplication implements AltitudeListener {

    private BrickletBarometer barometer;

    private final long updatePriodeInMilliseconds = 1;

    @Override
    public void deviceConnected(final TinkerforgeStackAgent tinkerforgeStackAgent, final Device device) {
        if (TinkerforgeDevice.getDevice(device) == TinkerforgeDevice.Barometer) {
            if (this.barometer == null) {
                this.barometer = (BrickletBarometer) device;
                this.barometer.addAltitudeListener(this);

                // Set averaging off & set update-period

                try {
                    this.barometer.setAveraging((short) 0, (short) 0, (short) 0);
                    this.barometer.setAltitudeCallbackPeriod(this.updatePriodeInMilliseconds);
                } catch (TimeoutException e) {
                    Logger.getLogger(AltitudeApplication.class.getName()).log(Level.SEVERE, null, e);
                } catch (final NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void deviceDisconnected(final TinkerforgeStackAgent tinkerforgeStackAgent, final Device device) {
        if (TinkerforgeDevice.areEqual(this.barometer, device)) {
            this.barometer.removeAltitudeListener(this);

            try {
                this.barometer.setAltitudeCallbackPeriod(0);
            } catch (TimeoutException e) {
                Logger.getLogger(AltitudeApplication.class.getName()).log(Level.SEVERE, null, e);
            } catch (NotConnectedException e) {
                e.printStackTrace();
            } finally {
                this.barometer = null;
            }
        }
    }

    @Override
    public void altitude(int i) {
        Controller.addBarometricAltitudeData(i);
    }

    @Override
    public boolean equals(Object o) {
        return this == o;
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
