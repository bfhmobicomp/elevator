package app;


import app.view.GUIApplication;
import com.uahnn.tinkerforge.tinker.agency.implementation.TinkerforgeStackAgency;
import com.uahnn.tinkerforge.tinker.agent.implementation.TinkerforgeStackAgent;
import com.uahnn.tinkerforge.tinker.core.implementation.TinkerforgeStackAddress;

/**
 * Created by Robin on 09.04.15
 */
public class Elevator {
    public static void main(final String[] args) throws Exception {
        final TinkerforgeStackAddress identifier = new TinkerforgeStackAddress(
                "localhost");
        final TinkerforgeStackAgent agent1 = TinkerforgeStackAgency
                .getInstance().getStackAgent(identifier);

        new Thread() {
            @Override
            public void run() {
                javafx.application.Application.launch(GUIApplication.class,
                        args);
            }
        }.start();
        final AltitudeApplication application = new AltitudeApplication();
        agent1.addApplication(application);
        System.in.read();
        agent1.removeApplication(application);

    }

}
